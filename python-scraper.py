import pandas as pd
from datetime import datetime

date = datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
df=pd.read_json('https://api.exchangerate-api.com/v4/latest/USD')

# drop by Name
df = df.drop(['terms','provider','WARNING_UPGRADE_TO_V6'], axis=1)
#add str to column
df['base'] = 'With 1 ' + df['base'].astype(str) +' buy'
#rename column
df.rename(columns={'base':'main curence USD'}, inplace = True)
#reorder columns
df = df.reindex(columns=['main curence USD','rates','date','time_last_updated'])
df.to_excel(r'USD' + date + '.xlsx')
print(df)